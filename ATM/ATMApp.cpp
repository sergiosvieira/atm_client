#include "ATMApp.h"

#include "ATMWelcomeDialog.h"

BOOL CATMApp::InitInstance()
{
	CWinApp::InitInstance();
	CATMWelcomeDialog frame;
	m_pMainWnd = &frame;
	INT_PTR result = frame.DoModal();
	return FALSE;
}
