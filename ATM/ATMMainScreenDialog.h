#pragma once

#include "afxwin.h"
#include "main_interface_resources.h"

class ATMMainScreenDialog : public CDialog
{
public:
	enum
	{
		IDD = MAIN_SCREEN
	};
	ATMMainScreenDialog(CWnd* pParent = nullptr);
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedBalance();
	afx_msg void OnBnClickedChange();
	afx_msg void OnBnClickedCash();
	afx_msg void OnBnClickedMini();
};

