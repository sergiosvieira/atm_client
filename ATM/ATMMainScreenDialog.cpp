#include "ATMMainScreenDialog.h"


ATMMainScreenDialog::ATMMainScreenDialog(CWnd* pParent) : CDialog(ATMMainScreenDialog::IDD, pParent)
{
}

BOOL ATMMainScreenDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	//m_control = new CATMWelcome(this);
	return TRUE;
}

BEGIN_MESSAGE_MAP(ATMMainScreenDialog, CDialog)
	ON_BN_CLICKED(IDC_BALANCE, &ATMMainScreenDialog::OnBnClickedBalance)
	ON_BN_CLICKED(IDC_CHANGE, &ATMMainScreenDialog::OnBnClickedChange)
	ON_BN_CLICKED(IDC_CASH, &ATMMainScreenDialog::OnBnClickedCash)
	ON_BN_CLICKED(IDC_MINI, &ATMMainScreenDialog::OnBnClickedMini)
END_MESSAGE_MAP()

void ATMMainScreenDialog::OnBnClickedBalance()
{

}

void ATMMainScreenDialog::OnBnClickedChange()
{

}

void ATMMainScreenDialog::OnBnClickedCash()
{

}

void ATMMainScreenDialog::OnBnClickedMini()
{

}
