#pragma once

#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include "afxwin.h"
#include "ATMWelcomeDialog.h"

using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace std;

class CATMWelcome
{
public:
	CATMWelcome(CATMWelcomeDialog* pDialog);
	void updatePassword(CString& value);
	void clearPassword();
	void make_request(http_client & client, method mtd, json::value const & jvalue);
	void enter();
protected:
	CATMWelcomeDialog* m_dialog;
	CEdit* m_edit = nullptr;
	CStatic* m_message = nullptr;
protected:
	CWnd * bindElement(int idc);
	void success(std::wstring userName);
	void fail();
};

