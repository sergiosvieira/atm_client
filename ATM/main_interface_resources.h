//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ATM.rc
//
#define MAIN_INTERFACE                  101
#define MAIN_SCREEN                     103
#define BALANCE                         104
#define IDC_WELCOME_MESSAGE             1001
#define IDC_PASSWORD                    1002
#define IDC_B1                          1004
#define IDC_B2                          1005
#define IDC_B3                          1006
#define IDC_B4                          1007
#define IDC_B5                          1008
#define IDC_B6                          1009
#define IDC_B7                          1010
#define IDC_B8                          1011
#define IDC_B9                          1012
#define IDC_CANCEL                      1013
#define IDC_CLEAN                       1014
#define IDC_ENTER                       1015
#define IDC_ERROR_MESSAGE               1016
#define IDC_MESSAGE                     1017
#define IDC_BALANCE                     1018
#define IDC_CASH                        1019
#define IDC_LIST1                       1019
#define IDC_CHANGE                      1020
#define IDC_MINI                        1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
