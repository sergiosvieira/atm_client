#pragma once
#include "afxwin.h"

#include "main_interface_resources.h"

class CATMWelcome;

class CATMWelcomeDialog : public CDialog
{
public:
	enum 
	{
		IDD = MAIN_INTERFACE
	};
	CATMWelcomeDialog(CWnd* pParent = nullptr);
	virtual BOOL OnInitDialog();	
	DECLARE_MESSAGE_MAP()
	/** Events **/
	afx_msg void OnButton1Clicked();
protected:
	CATMWelcome* m_control = nullptr;
public:
	afx_msg void OnButton2Click();
	afx_msg void OnBnClickedB3();
	afx_msg void OnBnClickedB4();
	afx_msg void OnBnClickedB5();
	afx_msg void OnBnClickedB6();
	afx_msg void OnBnClickedB7();
	afx_msg void OnBnClickedB8();
	afx_msg void OnBnClickedB9();
	afx_msg void OnClearClick();
	afx_msg void OnEnterClick();
};


