#include "ATMWelcome.h"

#include <cassert>
#include "ATMMainScreenDialog.h"


CATMWelcome::CATMWelcome(CATMWelcomeDialog* pDialog):
	m_dialog(pDialog)
{
	m_edit = (CEdit*)bindElement(IDC_PASSWORD);
	m_edit->SetLimitText(4);
	m_message = (CStatic*)bindElement(IDC_MESSAGE);
}

void CATMWelcome::updatePassword(CString & value)
{
	CString str = "";
	m_edit->GetWindowTextW(str);
	str += value;
	if (str.GetLength() < 5)
	{
		m_edit->SetWindowTextW(str);
	}
}

void CATMWelcome::clearPassword()
{
	m_edit->SetWindowTextW(L"");
}

void display_field_map_json(json::value & jvalue)
{
	if (!jvalue.is_null())
	{
	}
}

pplx::task<http_response> make_task_request(http_client & client,
	method mtd,
	json::value const & jvalue)
{
	return (mtd == methods::GET || mtd == methods::HEAD) ?
		client.request(mtd, L"/atm/service") :
		client.request(mtd, L"/atm/service", jvalue);
}

void CATMWelcome::make_request(http_client & client, method mtd, json::value const & jvalue)
{
	make_task_request(client, mtd, jvalue)
		.then([=](http_response response)
	{
		if (response.status_code() == status_codes::OK)
		{
			return response.extract_json();
		}
		json::value answer;
		answer[L"invalid_pin"] = json::value::boolean(true);
		return pplx::task_from_result(answer);
	}).then([=](pplx::task<json::value> previousTask)
	{
		json::value obj = previousTask.get();
		
		if (obj.has_field(utility::string_t(L"invalid_pin")))
		{
			this->fail();
		}
		else
		{
			this->success(obj[L"user_name"].as_string());
		}
		
	});
}

void CATMWelcome::enter()
{
	http_client client(L"http://localhost:34568/atm/service");
	json::value json;
	CString str = "";
	m_edit->GetWindowTextW(str);
	json[L"user"] = json::value::string(utility::string_t(str));
	make_request(client, methods::POST, json);
}

CWnd * CATMWelcome::bindElement(int idc)
{
	CWnd* result = m_dialog->GetDlgItem(idc);
	assert(result != nullptr);
	return result;
}

std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

void CATMWelcome::success(std::wstring userName)
{
	std::wostringstream ss;
	ss << "Client:"
		<< userName.c_str();
	m_message->SetWindowTextW(ss.str().c_str());
	ATMMainScreenDialog screen;
	screen.DoModal();
}

void CATMWelcome::fail()
{
	m_message->SetWindowTextW(L"Invalid PIN");
}