#include "ATMWelcomeDialog.h"

#include <cassert>
#include "ATMWelcome.h"


CATMWelcomeDialog::CATMWelcomeDialog(CWnd * pParent): CDialog(CATMWelcomeDialog::IDD, pParent)
{
}

BOOL CATMWelcomeDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_control = new CATMWelcome(this);
	return TRUE;
}

/** Events **/
BEGIN_MESSAGE_MAP(CATMWelcomeDialog, CDialog)
	ON_BN_CLICKED(IDC_B1, &CATMWelcomeDialog::OnButton1Clicked)
	ON_BN_CLICKED(IDC_B2, &CATMWelcomeDialog::OnButton2Click)
	ON_BN_CLICKED(IDC_B3, &CATMWelcomeDialog::OnBnClickedB3)
	ON_BN_CLICKED(IDC_B4, &CATMWelcomeDialog::OnBnClickedB4)
	ON_BN_CLICKED(IDC_B5, &CATMWelcomeDialog::OnBnClickedB5)
	ON_BN_CLICKED(IDC_B6, &CATMWelcomeDialog::OnBnClickedB6)
	ON_BN_CLICKED(IDC_B7, &CATMWelcomeDialog::OnBnClickedB7)
	ON_BN_CLICKED(IDC_B8, &CATMWelcomeDialog::OnBnClickedB8)
	ON_BN_CLICKED(IDC_B9, &CATMWelcomeDialog::OnBnClickedB9)
	ON_BN_CLICKED(IDC_CLEAN, &CATMWelcomeDialog::OnClearClick)
	ON_BN_CLICKED(IDC_ENTER, &CATMWelcomeDialog::OnEnterClick)
END_MESSAGE_MAP()

void CATMWelcomeDialog::OnButton1Clicked()
{
	m_control->updatePassword(CString(L"1"));
}

void CATMWelcomeDialog::OnButton2Click()
{
	m_control->updatePassword(CString(L"2"));
}

void CATMWelcomeDialog::OnBnClickedB3()
{
	m_control->updatePassword(CString(L"3"));
}

void CATMWelcomeDialog::OnBnClickedB4()
{
	m_control->updatePassword(CString(L"4"));
}

void CATMWelcomeDialog::OnBnClickedB5()
{
	m_control->updatePassword(CString(L"5"));
}

void CATMWelcomeDialog::OnBnClickedB6()
{
	m_control->updatePassword(CString(L"6"));
}

void CATMWelcomeDialog::OnBnClickedB7()
{
	m_control->updatePassword(CString(L"7"));
}

void CATMWelcomeDialog::OnBnClickedB8()
{
	m_control->updatePassword(CString(L"8"));
}

void CATMWelcomeDialog::OnBnClickedB9()
{
	m_control->updatePassword(CString(L"9"));
}

void CATMWelcomeDialog::OnClearClick()
{
	m_control->clearPassword();
}

void CATMWelcomeDialog::OnEnterClick()
{
	m_control->enter();
}